﻿namespace EVEMon.SDEToSQL.Importers
{
    internal interface IImporter
    {
        /// <summary>
        /// Imports the files.
        /// </summary>
        void ImportFiles();
    }
}