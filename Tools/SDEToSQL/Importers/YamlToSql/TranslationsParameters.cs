﻿namespace EVEMon.SDEToSQL.Importers.YamlToSQL
{
    internal class TranslationsParameters
    {
        public string TableName { get; set; }

        public string SourceTable { get; set; }

        public string ColumnName { get; set; }

        public string MasterID { get; set; }

        public string TcGroupID { get; set; }

        public string TcID { get; set; }
    }
}
