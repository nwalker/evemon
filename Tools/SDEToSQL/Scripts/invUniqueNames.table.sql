﻿IF OBJECT_ID('dbo.invUniqueNames', 'U') IS NOT NULL
DROP TABLE [dbo].[invUniqueNames]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[invUniqueNames](
	[itemID] [int] NOT NULL,
	[itemName] [nvarchar](200) NOT NULL,
	[groupID] [int] NULL,
 CONSTRAINT [invUniqueNames_PK] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]