﻿IF OBJECT_ID('dbo.warCombatZoneSystems', 'U') IS NOT NULL
DROP TABLE [dbo].[warCombatZoneSystems]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[warCombatZoneSystems](
	[solarSystemID] [int] NOT NULL,
	[combatZoneID] [int] NULL,
 CONSTRAINT [combatZoneSystems_PK] PRIMARY KEY CLUSTERED 
(
	[solarSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]