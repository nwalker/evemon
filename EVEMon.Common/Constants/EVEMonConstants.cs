namespace EVEMon.Common.Constants
{
    public static class EVEMonConstants
    {
        public const int ImplantSetNameMaxLength = 128;
        public const string UnknownText = "Unknown";
    }
}