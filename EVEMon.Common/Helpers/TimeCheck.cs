using System;
using EVEMon.Common.Constants;
using EVEMon.Common.Extensions;
using EVEMon.Common.Net;
using EVEMon.Common.Threading;

namespace EVEMon.Common.Helpers
{
    public delegate void TimeSynchronisationCallback(bool isSynchronised, DateTime serverTime, DateTime localTime);

    /// <summary>
    /// Ensures synchronization of local time to a know time source.
    /// </summary>
    public static class TimeCheck
    {
        /// <summary>
        /// Asynchronous method to determine if the user's clock is syncrhonized to BattleClinic time.
        /// </summary>
        /// <param name="callback"></param>
        /// <remarks>We are sending also a unique id to use for statistical purposes.</remarks>
        public static void CheckIsSynchronised(TimeSynchronisationCallback callback)
        {
            SyncState state = new SyncState(callback);
            Uri url = new Uri(String.Format(CultureConstants.InvariantCulture, "{0}{1}",
                NetworkConstants.BattleClinicBase, NetworkConstants.BatlleClinicTimeSynch));
            HttpWebService.DownloadStringAsync(url, SyncDownloadCompleted, state);
        }

        /// <summary>
        /// Callback method for synchronisation check. The user's clock is deemed to be in sync with BattleClinic time
        /// if it is no more than 60 seconds different to BattleClinic time as local time.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="userState"></param>
        private static void SyncDownloadCompleted(DownloadStringAsyncResult e, object userState)
        {
            SyncState state = (SyncState)userState;
            bool isSynchronised = true;
            DateTime serverTime = DateTime.MinValue;
            DateTime localTime = DateTime.Now;
            if (!String.IsNullOrEmpty(e.Result))
            {
                serverTime = e.Result.TimeStringToDateTime().ToLocalTime();
                double timediff = Math.Abs(serverTime.Subtract(localTime).TotalSeconds);
                isSynchronised = timediff < 60;
            }
            Dispatcher.Invoke(() => state.Callback(isSynchronised, serverTime, localTime));
        }

        /// <summary>
        /// Helper class for asyncronous time sync requests.
        /// </summary>
        private class SyncState
        {
            private readonly TimeSynchronisationCallback m_callback;

            /// <summary>
            /// Initializes a new instance of the <see cref="SyncState"/> class.
            /// </summary>
            /// <param name="callback">The callback.</param>
            internal SyncState(TimeSynchronisationCallback callback)
            {
                m_callback = callback;
            }

            /// <summary>
            /// Gets the callback.
            /// </summary>
            internal TimeSynchronisationCallback Callback
            {
                get { return m_callback; }
            }
        }
    }
}