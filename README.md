# **EVEMon**

A lightweight, easy-to-use standalone Windows application designed to assist you in keeping track of your EVE Online character progression.

For complete info on *How To* please visit the [wiki](https://bitbucket.org/BattleClinic/evemon/wiki) section

**Owner:** [BatlleClinic](https://bitbucket.org/BattleClinic)

**Admins:** [Jimi C](https://bitbucket.org/Desmont_McCallock)